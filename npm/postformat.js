'use strict'

const { promises: fsp } = require('node:fs')

const FN1_RX = /((?:^ *|[,=] )(?:async |export |return )?function [$]?\w+)(\((?:$|.*\) {(?:$|})))/gm
const FN2_RX = /(^ +(?:(?:async|get|set|static(?: async)?|) |[*])?(?=[$]?\w)[\w$]+)(\([^(]*\) {(?:},)?)$/gm
const IIFE_RX = /([^\n])\n(;\((?:async )?\(\) => {)/

;(async () => {
  const globs = await fsp
    .readFile('biome.json', 'utf8')
    .then(JSON.parse)
    .then((config) => config.files.include)
  for await (const entry of fsp.glob(globs)) {
    if (!(entry.endsWith('.js') || entry.endsWith('.mjs'))) continue
    await fsp.readFile(entry, 'utf8').then((contents) => {
      contents = contents.replace(FN1_RX, '$1 $2').replace(FN2_RX, '$1 $2').replace(IIFE_RX, '$1\n\n$2')
      return fsp.writeFile(entry, contents, 'utf8')
    })
  }
})()
