'use strict'

process.env.NODE_ENV = 'test'

const assert = require('node:assert/strict')
const assertx = require('./assertx')
const { after, beforeEach, describe, it } = require('node:test')
const { parseDocument } = require('htmlparser2')
const { selectAll, selectOne } = require('css-select')
const { configureLogger } = require(
  require.resolve('@antora/logger', {
    paths: [require.resolve('@antora/site-generator')],
  })
)
const ContentCatalog = require(
  require.resolve('@antora/content-classifier/lib/content-catalog', {
    paths: [require.resolve('@antora/site-generator')],
  })
)

function buildContentCatalog (playbook, files = []) {
  const catalog = new ContentCatalog(playbook)
  const contentVersionKeys = new Set()
  files.forEach((file) => {
    file = Object.assign({ asciidoc: {} }, file)
    const src = (file.src = Object.assign({ module: 'ROOT', family: 'page', relative: 'index.adoc' }, file.src))
    const { component, version } = src
    const contentVersionKey = `${version}@${component}`
    if (!contentVersionKeys.has(contentVersionKey)) {
      catalog.registerComponentVersion(component, version)
      contentVersionKeys.add(contentVersionKey)
    }
    catalog.addFile(file)
  })
  return catalog
}

function heredoc (literals, ...values) {
  const str =
    literals.length > 1
      ? values.reduce((accum, value, idx) => accum + value + literals[idx + 1], literals[0])
      : literals[0]
  const lines = str.trimRight().split(/^/m)
  if (lines.length < 2) return str
  if (lines[0] === '\n') lines.shift()
  const indentRx = /^ +/
  const indentSize = Math.min(...lines.filter((l) => l.startsWith(' ')).map((l) => l.match(indentRx)[0].length))
  return (indentSize ? lines.map((l) => (l.startsWith(' ') ? l.slice(indentSize) : l)) : lines).join('')
}

function parseHTML (html) {
  const dom = parseDocument(html.toString(), { lowerCaseTags: false, lowerCaseAttributeNames: false })
  dom.querySelectorAll = (query) => selectAll(query, dom)
  dom.querySelector = (query) => selectOne(query, dom)
  return dom
}

module.exports = {
  after,
  assert,
  assertx,
  beforeEach,
  buildContentCatalog,
  configureLogger,
  describe,
  heredoc,
  it,
  parseHTML,
}
