'use strict'

const assert = require('node:assert/strict')
const fs = require('node:fs')

const assertx = {
  contents: (actual, expected, msg) => {
    assertx.file(actual)
    assert.equal(fs.readFileSync(actual, 'utf8'), expected, msg)
  },
  doesNotHaveProperty: (actual, expected) =>
    assert(!(expected in actual), `Expected ${actual} to not have property '${expected}'`),
  empty: (actual, msg = 'Expected value to be empty') => assert.equal(actual.length, 0, msg),
  file: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isFile: () => false }
    }
    assert(stat.isFile(), `Expected value to be a file: ${actual}`)
  },
  hasProperties: (actual, expected, msg) =>
    assert.deepEqual(sliceObject(actual, ...Object.keys(expected)), expected, msg),
  notEmpty: (actual, msg = 'Expected value to not be empty') => assert.notEqual(actual.length, 0, msg),
  notPath: (actual) => {
    try {
      fs.accessSync(actual, fs.constants.F_OK)
      assert.fail(`Expected value to not be a path: ${actual}`)
    } catch {}
  },
  one: (actual, msg) => assert.equal(actual.length, 1, msg),
  sameFile: (actual, expected, msg) => {
    const expectedContents = fs.readFileSync(expected, 'utf8')
    assertx.contents(actual, expectedContents, `Expected contents of ${actual} to match contents of ${expected}`)
  },
}

function sliceObject (obj, ...keys) {
  const result = {}
  for (const k of keys) result[k] = obj[k]
  return result
}

module.exports = assertx
