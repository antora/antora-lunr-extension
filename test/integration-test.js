'use strict'

const { after, assert, assertx, beforeEach, describe, it, parseHTML } = require('./harness')
const fsp = require('node:fs/promises')
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const WORK_DIR = ospath.join(__dirname, 'work')

const generateSite = require('@antora/site-generator')

describe('generateSite()', () => {
  const cacheDir = ospath.join(WORK_DIR, '.cache/antora')
  const outputDir = ospath.join(WORK_DIR, 'public')
  const defaultPlaybookFile = ospath.join(FIXTURES_DIR, 'docs-site/antora-playbook.yml')
  const playbookFileWithCustomSupplementalUi = ospath.join(
    FIXTURES_DIR,
    'docs-site',
    'antora-playbook-with-custom-supplemental-ui.yml'
  )

  beforeEach(() => fsp.rm(outputDir, { recursive: true, force: true }))
  after(() => fsp.rm(WORK_DIR, { recursive: true, force: true }))

  it('should generate a site with a search index', async () => {
    const env = {}
    await generateSite(
      ['--playbook', defaultPlaybookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_PROVIDER')
    const searchIndexPath = ospath.join(outputDir, 'search-index.js')
    assertx.file(searchIndexPath)
    global.lunr = {}
    global.antoraSearch = {}
    global.antoraSearch.initSearch = function (lunr, index) {
      assert.equal(Object.keys(index.store.documents).length, 2)
      const documents = Object.entries(index.store.documents).map(([key, value]) => ({
        title: value.title,
        url: value.url,
      }))
      assert.equal(documents.length, 2)
      documents.sort((a, b) => a.title.localeCompare(b.title))
      assertx.hasProperties(documents[0], {
        title: 'Antora Lunr Extension',
        url: '/antora-lunr-extension/index.html',
      })
      assertx.hasProperties(documents[1], {
        title: 'Page Title',
        url: '/antora-lunr-extension/named-module/the-page.html',
      })
    }
    require(searchIndexPath)
    delete global.lunr
    delete global.antoraSearch
  })

  it('should insert script element with predefined data attributes', async () => {
    await generateSite(
      ['--playbook', defaultPlaybookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const startPageContents = await fsp.readFile(ospath.join(outputDir, 'antora-lunr-extension/index.html'))
    let doc = parseHTML(startPageContents)
    assertx.one(doc.querySelectorAll('#search-input'))
    assertx.one(doc.querySelectorAll('#search-ui-script'))
    let searchScript = doc.querySelector('#search-ui-script')
    assert.equal(searchScript.attribs['data-site-root-path'], '..')
    assert.equal(searchScript.attribs['data-stylesheet'], '../_/css/search.css')
    assert.equal(searchScript.attribs['data-snippet-length'], '100')
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr-extension/named-module/the-page.html')
    )
    doc = parseHTML(thePageContents)
    searchScript = doc.querySelector('#search-ui-script')
    assert.equal(searchScript.attribs['data-site-root-path'], '../..')
    assert.equal(searchScript.attribs['data-stylesheet'], '../../_/css/search.css')
  })

  it('should output lunr.js client/engine to js vendor directory of UI output folder', async () => {
    await generateSite(
      ['--playbook', defaultPlaybookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const expected = ospath.join(outputDir, '_/js/vendor/lunr.js')
    assertx.sameFile(expected, require.resolve('lunr/lunr.min.js'))
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr-extension/named-module/the-page.html')
    )
    const doc = parseHTML(thePageContents)
    assertx.one(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.js"]'))
  })

  it('should output vendored JS files to multiple destinations', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-destinations.yml')
    await generateSite(['--playbook', playbookFile, '--cache-dir', cacheDir, '--quiet'], {})
    const expectedA = ospath.join(outputDir, 'a', '_/js/vendor/lunr.js')
    assertx.sameFile(expectedA, require.resolve('lunr/lunr.min.js'))
    const expectedB = ospath.join(outputDir, 'b', '_/js/vendor/lunr.js')
    assertx.sameFile(expectedB, require.resolve('lunr/lunr.min.js'))
  })

  it('should output language support files to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-languages.yml')
    const env = {}
    await generateSite(['--playbook', playbookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'], env)
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.fr.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.de.js')),
    ]).then(Buffer.concat)
    assertx.contents(ospath.join(outputDir, '_/js/vendor/lunr-languages.js'), expectedContents.toString())
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr-extension/named-module/the-page.html')
    )
    const doc = parseHTML(thePageContents)
    assertx.one(doc.querySelectorAll('script[src="../../_/js/vendor/lunr-languages.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.stemmer.support.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.fr.js"]'))
  })

  it('should output language support files[ja] to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-languages-ja.yml')
    const env = {}
    await generateSite(['--playbook', playbookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'], env)
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/tinyseg.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.ja.js')),
    ]).then(Buffer.concat)
    assertx.contents(ospath.join(outputDir, '_/js/vendor/lunr-languages.js'), expectedContents.toString())
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr-extension/named-module/the-page.html')
    )
    const doc = parseHTML(thePageContents)
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.stemmer.support.js"]'))
    assertx.one(doc.querySelectorAll('script[src="../../_/js/vendor/lunr-languages.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/tinyseg.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.ja.js"]'))
  })

  it('should output language support files[th] to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-languages-th.yml')
    const env = {}
    await generateSite(['--playbook', playbookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'], env)
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/wordcut.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.th.js')),
    ]).then(Buffer.concat)
    assertx.contents(ospath.join(outputDir, '_/js/vendor/lunr-languages.js'), expectedContents.toString())
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr-extension/named-module/the-page.html')
    )
    const doc = parseHTML(thePageContents)
    assertx.one(doc.querySelectorAll('script[src="../../_/js/vendor/lunr-languages.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.stemmer.support.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/wordcut.js"]'))
    assertx.empty(doc.querySelectorAll('script[src="../../_/js/vendor/lunr.th.js"]'))
  })

  it('should allow extension to configure snippet length', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-snippet-length.yml')
    await generateSite(['--playbook', playbookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'], {})
    const startPageContents = await fsp.readFile(ospath.join(outputDir, 'antora-lunr-extension/index.html'))
    const searchScript = parseHTML(startPageContents).querySelector('#search-ui-script')
    assert.equal(searchScript.attribs['data-snippet-length'], '250')
  })

  it('should use existing search-scripts.hbs partial if present in UI', async () => {
    await generateSite(
      ['--playbook', playbookFileWithCustomSupplementalUi, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const startPageContents = await fsp.readFile(ospath.join(outputDir, 'antora-lunr-extension/index.html'))
    const searchScript = parseHTML(startPageContents).querySelector('#search-ui-script')
    assert.equal(searchScript.attribs['data-snippet-length'], '150')
    assert.equal(searchScript.attribs['data-stylesheet'], undefined)
  })

  it('should not generate search index or add scripts to pages if extension is not enabled', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-without-extension.yml')
    const env = {}
    await generateSite(['--playbook', playbookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'], env)
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_PROVIDER')
    assertx.notPath(ospath.join(outputDir, 'search-index.js'))
    const startPageContents = await fsp.readFile(ospath.join(outputDir, 'antora-lunr-extension/index.html'))
    const doc = parseHTML(startPageContents)
    assertx.empty(doc.querySelectorAll('#search-input'))
    assertx.empty(doc.querySelectorAll('#search-ui-script'))
  })

  it('should throw error if unknown options are specified in playbook', async () => {
    const playbookFile = ospath.join(FIXTURES_DIR, 'docs-site', 'antora-playbook-with-unknown-options.yml')
    const expected = new Error('Unrecognized options specified for @antora/lunr-extension: foo, yin')
    await assert.rejects(generateSite(['--playbook', playbookFile], {}), expected)
  })

  it('should output search.css to css directory of UI output folder', async () => {
    await generateSite(
      ['--playbook', defaultPlaybookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const actual = ospath.join(outputDir, '_/css/search.css')
    const expected = ospath.join(__dirname, '..', 'data', 'css', 'search.css')
    assertx.sameFile(actual, expected)
  })

  it('should output search-ui.js to js directory of UI output folder', async () => {
    await generateSite(
      ['--playbook', defaultPlaybookFile, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const actual = ospath.join(outputDir, '_/js/search-ui.js')
    const expected = ospath.join(__dirname, '..', 'data', 'js', 'search-ui.js')
    assertx.sameFile(actual, expected)
  })

  it('should preserve existing search.css', async () => {
    await generateSite(
      ['--playbook', playbookFileWithCustomSupplementalUi, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const actual = ospath.join(outputDir, '_/css/search.css')
    const expected = ospath.join(__dirname, 'fixtures', 'docs-site', 'custom-supplemental-ui', 'css', 'search.css')
    assertx.sameFile(actual, expected)
  })

  it('should preserve existing search-ui.js', async () => {
    await generateSite(
      ['--playbook', playbookFileWithCustomSupplementalUi, '--to-dir', outputDir, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const actual = ospath.join(outputDir, '_/js/search-ui.js')
    const expected = ospath.join(__dirname, 'fixtures', 'docs-site', 'custom-supplemental-ui', 'js', 'search-ui.js')
    assertx.sameFile(actual, expected)
  })
})
